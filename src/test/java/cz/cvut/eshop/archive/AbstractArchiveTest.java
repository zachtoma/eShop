package cz.cvut.eshop.archive;

import org.junit.Before;

import cz.cvut.eshop.TestData;
import cz.cvut.eshop.shop.Item;

abstract class AbstractArchiveTest {

	Item item;
	ItemPurchaseArchiveEntry itemArchive;

	@Before
	public void setUp() {
		item = TestData.createStandardItem();
		itemArchive = new ItemPurchaseArchiveEntry(item);
	}
}
