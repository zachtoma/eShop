package cz.cvut.eshop.shop;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class OrderTest {

    ShoppingCart shoppingCart = new ShoppingCart();
    Order order;
    Customer customer = new Customer(0);
    @Before
    public void fillShoppingCartAndCreateOrderAndAddCustomer () {
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        Item item2 = new StandardItem(2, "apple", 2.50f, "food", 5);
        shoppingCart.addItem(item);
        shoppingCart.addItem(item2);

        order = new Order(shoppingCart);

        order.setCustomer(customer);
    }

    @Test
    public void create_orderCreatedFromShoppingCardBefore_shouldSetLoyaltyPointToCustomer() {

        order.create();

        assertEquals(8, customer.getLoyaltyPoints());
    }

    @Test
    public void applyDiscount_usingSC() {
        order.create();
        order.applyDiscount();

        float discount = (float) (0.2 * 8);
        float price = 7 - discount;

        assertEquals(price, order.getTotalAmount(), 0.05f);

    }

    @Test
    public void getTotalAmount_usingSameSC_shouldBe7() {
        assertEquals(7f, order.getTotalAmount(), 0.05f);
    }

    @Test
    public void testConstructorOrderWithDate(){
        ShoppingCart cart = new ShoppingCart();
        Date date = new Date("2018/10/29");
        Order order = new Order(cart, date);

        assertEquals( date, order.getPurchaseOrder());
    }

    @Test
    public void testSetGetOrderState(){
        order.setState(13);
        assertEquals( 13, order.getState());
    }

    @Test
    public void testGetOrderCustomer(){
        assertEquals( customer, order.getCustomer());
    }

    @Test
    public void testSetGetOrderItems(){
        ArrayList<Item> a = new ArrayList<>();

        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        Item item2 = new StandardItem(2, "apple", 2.50f, "food", 5);
        Item item3 = new StandardItem(3, "orange", 7.50f, "food", 6);

        a.add(item);
        a.add(item2);
        a.add(item3);

        order.setItems(a);

        assertEquals( a, order.getItems());
    }


}