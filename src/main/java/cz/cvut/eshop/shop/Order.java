package cz.cvut.eshop.shop;

import java.util.ArrayList;
import java.util.Date;

/**
 * Order is created, when an user purchases the content of the shopping cart.
 * 
 */
public class Order {

    private ArrayList<Item> items;
    int state;
    private Date purchaseOrder;
    private Customer customer;
    private float totalDiscount;

    public Order(ShoppingCart cart, int state) {
        items = cart.getCartItems();
        this.state = state;
    }

    public Order(ShoppingCart cart) {
        items = cart.getCartItems();
        this.state = 0;
    }
    
    public Order(ShoppingCart cart, Date purchaseOrder) {
        items = cart.getCartItems();
        this.state = 0;
        this.purchaseOrder = purchaseOrder;
    }
    
    
    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> goods) {
        this.items = goods;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getPurchaseOrder() {
        return purchaseOrder;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    

    public void create() {
        int loyaltyPoints = 0;
        // spocitat vernostni body za polozky v nak. voziku
        for (Item item : items) {
            if (item instanceof StandardItem)
            {
                StandardItem standardItem = (StandardItem)item;
                loyaltyPoints += standardItem.getLoyaltyPoints();
            }
        }
        if(this.customer == null) {
            return;
        }
        // priradit je zakaznikovi
        this.customer.addLoayltyPoint(loyaltyPoints);
    }

    public void applyDiscount() {
        int loyaltyPoints = this.customer.getLoyaltyPoints();
        // 1 bod = 0.2 sleva
        this.totalDiscount = (float) (0.2 * loyaltyPoints);
    }

    public float getTotalAmount() {
        float totalAmount = 0;
        // spocitat vernostni body za polozky v nak. voziku
        for (Item item : items) {
            totalAmount += item.getPrice();
        }
        return totalAmount - this.totalDiscount;
    }
}
